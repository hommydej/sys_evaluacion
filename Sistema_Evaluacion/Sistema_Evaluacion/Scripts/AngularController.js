﻿var app = angular.module('SistemaEvaluacion', []);


//START ANGULARJS REPORT CONTROLLER
app.controller('ReportController', function ($scope, $http, $location, $window) {

    getSessionsByProfessorCod();
    getCourses();

    function getSessionsByProfessorCod() {
        $http({
            method: "GET",
            url: "/Session/ByProfessorCod"
        }).then(function (response) {
            $scope.sessions = response.data;
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    //BUSCAMOS EL ESTUDIANTE EN BASE AL CODIGO DE LA SESIÓN SELECCIONADA
    $('#sessionCod').change(function () {

        var sessionCod = $('#sessionCod').val();
        console.log(sessionCod);
        $http({
            method: "GET",
            url: "/Student/GetBySession?SessionCod=" + sessionCod
        }).then(function (response) {
            $scope.students = response.data;
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    });

    //BUSCAMOS LA NOTA DEL ESTUDIANTE SELECCIONADO EN BASE A SU CODIGO
    $('#studentCod').change(function () {

        var studentCod = $('#studentCod').val();

        $http({
            method: "GET",
            url: "/Report/GetStudentSpecificGradeReport?studentCod=" + studentCod
        }).then(function (response) {
            $scope.studentGrades = response.data;
            console.log($scope.studentGrades);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    });

    function getCourses() {
        $http({
            method: "GET",
            url: "/Course/GetAllCourses"
        }).then(function (response) {
            $scope.courses = response.data;
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.getStudentsByCourse = function (courseCod) {

        $http({
            method: "POST",
            url: "/Student/GetStudentsByCourse",
            data: { 'courseCod': courseCod.CourseCod }
        }).then(function (response) {
            $scope.students = response.data;
            console.log($scope.students);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    };


    $scope.getStudentGradeReport = function (studentCod, courseCod, sessionCod) {

        $http({
            method: "POST",
            url: "/Report/GetStudentGradeReport",
            data: {
                'studentCod': studentCod.StudentCod,
                'courseCod': courseCod.CourseCod
            }
        }).then(function (response) {
            $scope.gradesDetails = response.data;
            console.log($scope.gradesDetails);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    };

});
//END ANGULARJS REPORT CONTROLLER


//START ANGULARJS LAYOUT CONTROLLER
app.controller('LayoutController', function ($scope, $http, $location, $window) {

    //getModules();

    function getModules() {
        $http({
            method: "GET",
            url: "/Modules/GetModules"
        }).then(function (response) {
            $scope.modules = response.data;
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.logOut = function () {

        swal({
            title: "Cerrar Sesión",
            text: "Está seguro que desea cerrar la sesión?",
            icon: "warning",
            allowOutsideClick: false,
            buttons: [
                'No',
                'Si, salir'
            ],
            dangerMode: true
        }).then(function (isConfirm) {
            if (isConfirm) {
                $http({
                    method: "GET",
                    url: "/Login/LogOut"
                }).then(function (response) {
                    if (response) {
                        window.location.href = "/Login";
                    }
                }, function (error) {
                    swal('Error', error.data, 'error');
                });
            }
        });
    };

});
//END ANGULARJS LAYOUT CONTROLLER


//START ANGULARJS EVALUATION CONTROLLER
app.controller('EvaluationController', function ($scope, $http, $location, $window) {

    let studentCod;

    getSessionsByProfessorCod();

    function getSessionsByProfessorCod() {
        $http({
            method: "GET",
            url: "/Session/ByProfessorCod"
        }).then(function (response) {
            $scope.sessions = response.data;
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.getSessionInformationModal = function (sessionInfo) {
        $scope.sessionInformationModal = sessionInfo;
    };

    function getCompetencesWithIndicators(sessionInfo) {

        $http({
            method: "GET",
            url: "/Competence/GetCompetencesWithIndicators?subjectCod=" + sessionInfo.SubjectCod
        }).then(function (response) {
            $scope.competencesAndIndicators = response.data;
            console.log($scope.competencesAndIndicators);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.getSubjectSessionBySubjectCod = function (sessionInfo) {

        $http({
            method: "GET",
            url: "/Subject/SubjectSessionBySubjectCod?subjectCod=" + sessionInfo.SubjectCod
        }).then(function (response) {
            $scope.subject = response.data;
            getSubjectCourseByCourseCod($scope.subject);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    };

    function getSubjectCourseByCourseCod(subjectInfo) {

        $http({
            method: "GET",
            url: "/Course/SubjectCourseByCourseCod?courseCod=" + subjectInfo[0].CourseCod
        }).then(function (response) {
            $scope.course = response.data;
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.openSessionStudentsModal = function (sessionInfo) {
        $('#sessionStudentsModal').modal('toggle');
        getStudentsBySession(sessionInfo);
    };

    $scope.openEvaluationModal = function (sessionInfo, student) {
        studentCod = student.StudentCod;
        getCompetencesWithIndicators(sessionInfo);
        $('#evaluationModal').modal('toggle');
    };

    //START FUNCTION
    $scope.saveGrades = function (subject) {

        let firstIndicatorGrades = 0;
        let secondIndicatorGrades = 0;
        let firstIndicatorFinalCalification = 0;
        let secondIndicatorFinalCalification = 0;

        firstIndicatorGrades = $('input[id=firstIndicatorGrades]').map(function () {
            return this.value;
        }).get();    

        secondIndicatorGrades = $('input[id=secondIndicatorGrades]').map(function () {
            return this.value;
        }).get();

        for (let i = 0; i < firstIndicatorGrades.length; i++) {
            firstIndicatorFinalCalification += parseInt(firstIndicatorGrades[i]);
        }

        for (let i = 0; i < secondIndicatorGrades.length; i++) {
            secondIndicatorFinalCalification += parseInt(secondIndicatorGrades[i]);
        }

        let firstIndicatorAverage  = firstIndicatorFinalCalification / firstIndicatorGrades.length;
        let secondIndicatorAverage = secondIndicatorFinalCalification / secondIndicatorGrades.length;


        let competenceSum = firstIndicatorAverage + secondIndicatorAverage;

        let competenceAverageResult = competenceSum / 2;

        let competenceAverageResultRounded = Math.trunc(competenceAverageResult);

        console.log(competenceAverageResultRounded);
        saveStudentGrade(competenceAverageResultRounded, subject);

    };
    //END FUNCTION

    function saveStudentGrade(studentGradeRounded, subject) {
        studentDetails = {};
        studentDetails.StudentCod = studentCod;
        studentDetails.Grade      = studentGradeRounded;
        studentDetails.SessionCod = $scope.sessionInformationModal.SessionCod;
        studentDetails.SubjectCod = subject.SubjectCod;
        debugger;
        $http({
            method: "POST",
            url: "/Student/SaveStudentGrade",
            data: JSON.stringify(studentDetails)
        }).then(function (response) {
            swal('Éxito', "Calificación guardada", 'success');
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    function getStudentsBySession(sessionInfo) {
        $http({
            method: "GET",
            url: "/Student/GetBySession?SessionCod=" + sessionInfo.SessionCod
        }).then(function (response) {
            $scope.students = response.data;
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

});
//END ANGULARJS EVALUATION CONTROLLER

//START ANGULARJS LOGIN CONTROLLER
app.controller('LoginController', function ($scope, $http, $location, $window) {

    $scope.login = function () {

        $http({
            method: "POST",
            url: "/Login/Login",
            data: {
                'userCod': $scope.codigoLogin,
                'password': $scope.passwordLogin
            }
        }).then(function (response) {
            if (response.data === "1") {
                window.location.href = "/Home";
            }
            else {
                swal("Error", "Usuario o Contraseña Incorrectos", "error");
            }
        }, function (error) {
            //swal('Error', error.data, 'error');
        });
    };

});
//END ANGULARJS LOGIN CONTROLLER


//START ANGULARJS PROFESSOR CONTROLLER
app.controller('ProfessorCreateController', function ($scope, $http, $location, $window) {

    //CALLS
    getProfessorRandomCode();

    function getProfessorRandomCode() {
        $http({
            method: "GET",
            url: "/Professor/RandomNumber"
        }).then(function (response) {
            $scope.professorRandomCode = response.data;
            $("#professorCode").val($scope.professorRandomCode);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.createProfessor = function () {

        $scope.newProfessor = {};

        $scope.newProfessor.Name = $scope.professorName;
        $scope.newProfessor.LastName = $scope.professorLastName;
        $scope.newProfessor.DateOfBirth = $scope.professorDateOfBirth;
        $scope.newProfessor.Identification = $scope.professorIdentification;
        $scope.newProfessor.TelephoneNumber = $scope.professorTelephoneNumber;
        $scope.newProfessor.CellPhoneNumber = $scope.professorCellPhoneNumber;
        $scope.newProfessor.Gender = $scope.professorGender;
        $scope.newProfessor.Address = $scope.professorAddress;
        $scope.newProfessor.City = $scope.professorCity;
        $scope.newProfessor.CountryOfBirth = $scope.professorCountryOfBirth;
        $scope.newProfessor.ProfessorCod = $scope.professorRandomCode;
        $scope.newProfessor.Password = $scope.professorPassword;
        $scope.newProfessor.UserType = 2;


        $http({
            method: "POST",
            url: "/Professor/CreateProfessor",
            data: JSON.stringify($scope.newProfessor)
        }).then(function (response) {
            swal("Éxito", response.data, "success");
            location.reload();
        }, function (error) {
            swal('Error', response.data, 'error');
        });
    };
});

//START AMGULARJS PROFESSOR CONTROLLER
app.controller('ViewAllProfessorController', function ($scope, $http, $location, $window) {

    //CALLS
    getProfessors();

    function getProfessors() {
        $http({
            method: "GET",
            url: "/Professor/GetAllProfessors"
        }).then(function (response) {
            $scope.professors = response.data;
            console.log($scope.professors);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.getProfessorInformationModal = function (professor) {

        $scope.professorInformationModal = professor;
    };

    $scope.openProfessorModifyModal = function () {
        $('#professorModifyModal').modal('toggle');

    };

    $scope.editProfessor = function (professorData) {

        $scope.professorEdit = professorData;

        $http({
            method: "POST",
            url: "/Professor/EditProfessor",
            data: JSON.stringify($scope.professorEdit)
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

    $scope.deleteProfessor = function (professorId) {

        $http({
            method: "GET",
            url: "/Professor/DeleteProfessor?professorId=" + professorId
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

    $scope.changeProfessorStatus = function (professorId) {

    };

});
//END ANGULARJS PROFESSOR CONTROLLER



//START AMGULARJS ADMIN CONTROLLER
app.controller('ViewAllAdminController', function ($scope, $http, $location, $window) {

    //CALLS
    getAdmins();

    function getAdmins() {
        $http({
            method: "GET",
            url: "/User/GetAll"
        }).then(function (response) {
            $scope.admins = response.data;
            console.log($scope.admins);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.getAdminInformationModal = function (admin) {

        $scope.adminInformationModal = admin;
    };

    $scope.openAdminModifyModal = function () {
        $('#adminModifyModal').modal('toggle');

    };

    $scope.editProfessor = function (professorData) {

        $scope.professorEdit = professorData;

        $http({
            method: "POST",
            url: "/Professor/EditProfessor",
            data: JSON.stringify($scope.professorEdit)
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

    $scope.deleteProfessor = function (professorId) {

        $http({
            method: "GET",
            url: "/Professor/DeleteProfessor?professorId=" + professorId
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

    $scope.changeProfessorStatus = function (professorId) {

    };

});


app.controller('AdminCreateController', function ($scope, $http, $location, $window) {

    //CALLS
    getAdminRandomCode();

    function getAdminRandomCode() {
        $http({
            method: "GET",
            url: "/User/RandomNumber"
        }).then(function (response) {
            $scope.adminRandomCode = response.data;
            $("#adminCode").val($scope.adminRandomCode);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.createAdmin = function () {

        $scope.newAdminUser = {};

        $scope.newAdminUser.CodUser      = $scope.adminRandomCode;
        $scope.newAdminUser.Name         = $scope.adminName;
        $scope.newAdminUser.LastName     = $scope.adminLastName;
        $scope.newAdminUser.Password     = $scope.adminPassword;
        $scope.newAdminUser.Accesos      = 0;

        $http({
            method: "POST",
            url: "/User/CreateUser",
            data: JSON.stringify($scope.newAdminUser)
        }).then(function (response) {
            swal("Éxito", response.data, "success");
            location.reload();
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    };
});
//END ANGULARJS ADMIN CONTROLLER


//START ANGULARJS COURSE CONTROLLER
app.controller('CourseCreateController', function ($scope, $http, $location, $window) {

    //CALLS
    getProfessorInCharge();
    getCourseRandomCode();

    function getCourseRandomCode() {
        $http({
            method: "GET",
            url: "/Course/RandomNumber"
        }).then(function (response) {
            $scope.courseRandomCode = response.data;
            $("#courseCode").val($scope.courseRandomCode);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    function getProfessorInCharge() {
        $http({
            method: "GET",
            url: "/Professor/GetAllProfessors"
        }).then(function (response) {
            $scope.professors = response.data;
            console.log($scope.professors);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    function updateAssignedProfessorFlag() {

        $http({
            method: "GET",
            url: "/Professor/UpdateAssignedProfessorFlag?professorId=" + $scope.courseProfessorInCharge.ProfessorCod
        }).then(function (response) {
            console.log("hola");
        }, function (error) {
            //swal('Error', error.data, 'error');
        });
    }

    function addStudentsToCourse(courseCod) {

        var elements = {}, studentsCode = [];

        var i;
        for (i = 0; i < $scope.student.length; i++) {
            studentsCode.push({ StudentCod: $scope.student[i].StudentCod, CourseCod: courseCod });
        }

        $http({
            method: "POST",
            url: "/Course/AddStudentsToCourse",
            data: JSON.stringify(studentsCode)
        }).then(function (response) {
            location.reload();
        }, function (error) {
            //swal('Error', error.data, 'error');
        });
    }

    $scope.goBack = function () {
        window.location.href = "/Course/ViewAll";
    };


    $scope.createCourse = function () {

        $scope.newCourse = {};

        $scope.newCourse.CourseCod = $scope.courseRandomCode;
        $scope.newCourse.Name = $scope.courseName;

        $http({
            method: "POST",
            url: "/Course/CreateCourse",
            data: JSON.stringify($scope.newCourse)
        }).then(function (response) {
            //updateAssignedProfessorFlag();
            //addStudentsToCourse($scope.courseRandomCode);
            location.reload();
        }, function (error) {
            //swal('Error', error.data, 'error');
        });
    };

});

app.controller('ViewAllCoursesController', function ($scope, $http, $location, $window) {

    //CALLS
    getCourses();

    function getCourses() {
        $http({
            method: "GET",
            url: "/Course/GetAllCourses"
        }).then(function (response) {
            $scope.courses = response.data;
            console.log($scope.professors);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.getSessionsBySubject = function (subject) {
        $http({
            method: "GET",
            url: "/Session/GetSessionsBySubject?subjectCod=" + subject.SubjectCod
        }).then(function (response) {
            debugger;
            $scope.sessions = response.data;
            console.log($scope.professors);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    };

    function getIndicatorRandomCode() {
        $http({
            method: "GET",
            url: "/Indicator/RandomNumber"
        }).then(function (response) {
            $scope.indicatorRandomCode = response.data;
            $("#indicatorRandomCode").val($scope.indicatorRandomCode);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.openIndicatorModalInformation = function () {
        $('#indicatorModalInformation').modal('toggle');
    };

    $scope.getCompetenceInformationModal = function (competence) {
        $scope.competenceInformationModal = competence;
    };

    $scope.getCourseInformationModal = function (course) {
        $scope.courseInformationModal = course;
    };

    $scope.getIndicatorInformationModal = function (indicator) {
        debugger;
        $scope.indicatorInformationModal = indicator;
    };

    $scope.openCompetenceModalInformation = function (competenceInfo) {
        $('#competenceModalInformation').modal('toggle');
        getIndicatorsByCompetenceCod(competenceInfo);
    };

    $scope.openCreateIndicatorModal = function () {
        $('#newIndicatorModal').modal('toggle');
        getIndicatorRandomCode();
    };

    $scope.openCourseSubjectsModal = function (courseInfo) {
        $('#courseSubjectsModal').modal('toggle');
        getSubjectsByCourseId(courseInfo.CourseCod);
    };

    $scope.openCreateSubjectsModal = function (courseInfo) {
        $('#subjectCreateModal').modal('toggle');

        $http({
            method: "GET",
            url: "/Subject/RandomNumber"
        }).then(function (response) {
            $scope.subjectCod = response.data;
            $("#subjectRandomCod").val($scope.subjectCod);
        }, function (error) {
            swal('Error', error.data, 'error');
        });

    };

    $scope.openCreateSessionModal = function () {
        $http({
            method: "GET",
            url: "/Session/RandomNumber"
        }).then(function (response) {
            $scope.sessionCod = response.data;
            $("#sessionRandomCod").val($scope.sessionCod);
        }, function (error) {
            swal('Error', error.data, 'error');
        });

        $('#newSessionModal').modal('toggle');
    };

    $scope.openModifySubjectsModal = function (subjectInfo) {
        $('#subjectModifyModal').modal('toggle');
        $scope.subjectInformationModal = subjectInfo;
    };

    $scope.openCourseModifyModal = function () {
        $('#courseModifyModal').modal('toggle');
    };

    function getSubjectsByCourseId(courseCod) {
        $http({
            method: "GET",
            url: "/Subject/GetByCourseId?courseCod=" + courseCod
        }).then(function (data) {
            $scope.subjects = data.data;
            console.log($scope.subjects);
        }, function (error) {
            swal("Error", error, "error");
        });
    }


    $scope.generateSessionName = function (courseInformation, subjectInformation) {

        //OBSERVACIÓN
        var timefrom = new Date();
        var fromTime = $('#fromInputTime').val().split(":");
        timefrom.setHours((parseInt(fromTime[0]) - 1 + 12) % 12);
        timefrom.setMinutes(parseInt(fromTime[1]));

        var timeto = new Date();
        var toTime = $('#toTimeInputTime').val().split(":");
        timeto.setHours((parseInt(toTime[0]) - 1 + 12) % 12);
        timeto.setMinutes(parseInt(toTime[1]));

        if (timeto < timefrom) {
            swal('Aviso', 'La hora final debe ser mayor a la inicial', 'info');
            return;
        }

        var dayArr = $('#daysSelect').val();

        if (dayArr === undefined && fromTime === '' && toTime === '') {
            swal('Aviso', 'Debe completar el horario y al menos un día', 'info');
            return;
        } else if (fromTime === '' || toTime === '') {
            swal('Aviso', 'Debe completar el horario', 'info');
            return;
        } else if (dayArr === undefined || dayArr === null) {
            swal('Aviso', 'Debe seleccionar al menos un día', 'info');
            return;
        }

        if (dayArr.length === 5)
            dayArr = 'L-V';

        $scope.name = courseInformation.Name + ' - ' + subjectInformation.Name + ' - ' + dayArr + ' - ' + fromTime + ' a ' + toTime;

        newSession(subjectInformation);
        $('#newSessionModal').modal('toggle');
    };

    $scope.getProfessors = function () {
        $http({
            method: "GET",
            url: "/Professor/GetAllProfessors"
        }).then(function (response) {
            $scope.professors = response.data;
        }, function (error) {
            swal(error.data, error.data, "error");
        });
    };

    $scope.openCourseCompetences = function () {
        $('#competencesModal').modal('toggle');
    };

    $scope.openIndicatorsModal = function (competenceInfo) {
        $('#indicatorsModal').modal('toggle');
        getIndicatorsByCompetenceCod(competenceInfo);
    };

    function getIndicatorsByCompetenceCod(competenceInfo) {
        $http({
            method: "GET",
            url: "/Indicator/GetIndicatorsByCompetenceCod?competenceCod=" + competenceInfo.CompetenceCod
        }).then(function (response) {
            $scope.indicators = response.data;
        }, function (error) {
            swal(error.data, error.data, "error");
        });
    }

    $scope.getCompetencesBySubjectCod = function (subjectInfo) {
        $http({
            method: "GET",
            url: "/Competence/BySubjectCod?subjectCod=" + subjectInfo.SubjectCod
        }).then(function (response) {
            $scope.competences = response.data;
        }, function (error) {
            swal(error.data, error.data, "error");
        });
    };

    $scope.openNewCompetenceModal = function () {
        $('#newCompetenceModal').modal('toggle');
        getCompetenceRandomCode();
    };

    function getCompetenceRandomCode() {
        $http({
            method: "GET",
            url: "/Competence/RandomNumber"
        }).then(function (response) {
            $scope.competenceRandomCode = response.data;
            $("#competenceRandomCod").val($scope.competenceRandomCode);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }


    $scope.newCompetence = function (subjectInfo) {

        $scope.newCompetence = {};

        $scope.newCompetence.CompetenceCod = $scope.competenceRandomCode;
        $scope.newCompetence.Name          = $scope.competenceName;
        $scope.newCompetence.SubjectCod    = subjectInfo.SubjectCod;

        $http({
            method: "POST",
            url: "/Competence/CreateCompetence",
            data: JSON.stringify($scope.newCompetence)
        }).then(function (response) {
            swal("Éxito", response.data, "success");
            $('#newCompetenceModal').modal('toggle');
            $scope.competenceName = '';
        }, function (error) {
            swal("Error", error.data, "error");
        });
    };

    function newSession(subjectInfo) {

        console.log(subjectInfo);

        $scope.newSession = {};

        $scope.newSession.SessionCod = $scope.sessionCod;
        $scope.newSession.SubjectCod = subjectInfo.SubjectCod;
        $scope.newSession.ProfessorCod = $scope.ProfessorCod;
        $scope.newSession.Name = $scope.name;

        $http({
            method: "POST",
            url: "/Session/Create",
            data: JSON.stringify($scope.newSession)
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    }

    $scope.createIndicator = function (competenceInfo) {

        $scope.newIndicator = {};

        $scope.newIndicator.IndicatorCod  = $scope.indicatorRandomCode;
        $scope.newIndicator.Name          = $scope.indicatorName;
        $scope.newIndicator.CompetenceCod = competenceInfo.CompetenceCod;

        $http({
            method: "POST",
            url: "/Indicator/Create",
            data: JSON.stringify($scope.newIndicator)
        }).then(function (response) {
            swal('Éxito', response.data, 'success');
            $('#newIndicatorModal').modal('toggle');
            $scope.indicatorName = '';
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    };

    $scope.newSubject = function (courseInfo) {

        $scope.newSubjectObj = {};

        $scope.newSubjectObj.subjectCod = $scope.subjectCod;
        $scope.newSubjectObj.Name = $scope.name;
        $scope.newSubjectObj.CourseCod = courseInfo.CourseCod;

        $http({
            method: "POST",
            url: "/Subject/Create",
            data: JSON.stringify($scope.newSubjectObj)
        }).then(function (response) {
            swal('Éxito', response.data, "success");
            $('#subjectCreateModal').modal('toggle');
        }, function (error) {
            swal('Error', error.data, "error");
        });
    };

    $scope.editIndicator = function (indicatorData) {

        $scope.editIndicatorObj = indicatorData;

        $http({
            method: "POST",
            url: "/Indicator/Edit",
            data: JSON.stringify($scope.editIndicatorObj)
        }).then(function (response) {
            swal('Éxito', response.data.status, 'success');
            $('#indicatorModalInformation').modal('toggle');
        }, function (error) {
            swal("Error", error.data.status, "error");
        });
    };

    $scope.editCompetence = function (competenceData) {

        $scope.editCompetenceObj = competenceData;

        $http({
            method: "POST",
            url: "/Competence/Edit",
            data: JSON.stringify($scope.editCompetenceObj)
        }).then(function (response) {
            swal('Éxito', response.data.status, 'success');
            $('#competenceModalInformation').modal('toggle');
        }, function (error) {
            swal("Error", error.data.status, "error");
        });
    };

    $scope.refreshIndicatorsByCompetence = function (competenceInfo) {

        $http({
            method: "GET",
            url: "/Indicator/GetIndicatorsByCompetenceCod?competenceCod=" + competenceInfo.CompetenceCod
        }).then(function (response) {
            $scope.indicators = response.data;
        }, function (error) {
            swal(error.data, error.data, "error");
        });
    };

    $scope.refresh = function (courseInfo) {
        getSubjectsByCourseId(courseInfo.CourseCod);
    };

    function getRefreshSessionsBySubjectId(subjectInfo) {
        $http({
            method: "GET",
            url: "/Session/GetSessionsBySubject?subjectCod=" + subjectInfo.SubjectCod
        }).then(function (response) {
            $scope.sessions = response.data;
            swal(response.data, response.data, "success");
        }, function (error) {
            swal(error.data, error.data, "error");
        });
    }

    $scope.refreshCompetences = function (subjectInfo) {
        $http({
            method: "GET",
            url: "/Competence/GetCompetencesBySubjectCod?subjectCod=" + subjectInfo.SubjectCod
        }).then(function (response) {
            $scope.competences = response.data;
        }, function (error) {
            swal(error.data, error.data, "error");
        });
    };

    $scope.refreshGetSessionsBySubject = function (subjectInfo) {
        getRefreshSessionsBySubjectId(subjectInfo);
    };

    $scope.editCourse = function (courseData) {

        $scope.editCourse = courseData;

        $http({
            method: "POST",
            url: "/Course/EditCourse",
            data: JSON.stringify($scope.courseEdit)
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

    $scope.deleteCourse = function (courseId) {

        $http({
            method: "GET",
            url: "/Course/DeleteCourse?courseId=" + courseId
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };


});
//END ANGULARJS COURSE CONTROLLER


//START ANGULARJS STUDENT CONTROLLER 
app.controller('StudentCreateController', function ($scope, $http, $location, $window) {

    //CALLS
    getStudentRandomCode();
    getSessions();

    function getSessions() {
        $http({
            method: "GET",
            url: "/Session/GetSessions"
        }).then(function (response) {
            $scope.sessions = response.data;
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    function getStudentRandomCode() {
        $http({
            method: "GET",
            url: "/Student/RandomNumber"
        }).then(function (response) {
            $scope.studentRandomCode = response.data;
            $("#studentCode").val($scope.studentRandomCode);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.createStudent = function () {
        $scope.newStudent = {};
        $scope.newStudent.SessionsCod = '';

        //iterate sessions selections
        for (var i = 0; i < $scope.Session.length; i++) {
            $scope.newStudent.SessionsCod += `${$scope.Session[i].SessionCod},`;
        }
    
        $scope.newStudent.StudentCod = $scope.studentRandomCode;
        $scope.newStudent.Name = $scope.studentName;
        $scope.newStudent.LastName = $scope.studentLastName;
        $scope.newStudent.DateOfBirth = $scope.studentDateOfBirth;
        $scope.newStudent.Gender = $scope.studentGender;
        $scope.newStudent.Address = $scope.studentAddress;      

        $http({
            method: "POST",
            url: "/Student/CreateStudent",
            data: JSON.stringify($scope.newStudent)
        }).then(function (response) {
            swal('Éxito', response.data, 'success');
            setTimeout(function () { location.reload(); }, 4000);      
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    };
});


app.controller('ViewAllStudentsController', function ($scope, $http, $location, $window) {

    //CALLS
    getStudents();

    function getStudents() {
        $http({
            method: "GET",
            url: "/Student/GetAllStudents"
        }).then(function (response) {
            $scope.students = response.data;
            console.log($scope.students);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.getStudentInformationModal = function (student) {

        $scope.studentInformationModal = student;
    };

    $scope.openStudentModifyModal = function () {
        $('#studentModifyModal').modal('toggle');

    };

    $scope.editStudent = function (studentData) {

        $scope.studentEdit = studentData;

        $http({
            method: "POST",
            url: "/Student/EditStudent",
            data: JSON.stringify($scope.studentEdit)
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

    $scope.deleteStudent = function (studentId) {

        $http({
            method: "GET",
            url: "/Student/DeleteStudent?studentId=" + studentId
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

});
//END ANGULARJS STUDENT CONTROLLER



//START ANGULARJS AREA CONTROLLER 
app.controller('AreaCreateController', function ($scope, $http, $location, $window) {

    //CALLS
    getAreaRandomCode();

    function getAreaRandomCode() {
        $http({
            method: "GET",
            url: "/Area/RandomNumber"
        }).then(function (response) {
            $scope.areaRandomCode = response.data;
            $("#areaCode").val($scope.areaRandomCode);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.createArea = function () {

        $scope.newArea = {};

        $scope.newArea.AreaCod = $scope.areaRandomCode;
        $scope.newArea.Name = $scope.areaName;

        $http({
            method: "POST",
            url: "/Area/CreateArea",
            data: JSON.stringify($scope.newArea)
        }).then(function (response) {
            location.reload();
        }, function (error) {
            //swal('Error', error.data, 'error');
        });
    };
});


app.controller('ViewAreaStudentsController', function ($scope, $http, $location, $window) {

    //CALLS
    getStudents();

    function getStudents() {
        $http({
            method: "GET",
            url: "/Student/GetAllStudents"
        }).then(function (response) {
            $scope.students = response.data;
            console.log($scope.students);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.getStudentInformationModal = function (student) {

        $scope.studentInformationModal = student;
    };

    $scope.openStudentModifyModal = function () {
        $('#studentModifyModal').modal('toggle');

    };

    $scope.editStudent = function (studentData) {

        $scope.studentEdit = studentData;

        $http({
            method: "POST",
            url: "/Student/EditStudent",
            data: JSON.stringify($scope.studentEdit)
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

    $scope.deleteStudent = function (studentId) {

        $http({
            method: "GET",
            url: "/Student/DeleteStudent?studentId=" + studentId
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

});
//END ANGULARJS AREA CONTROLLER


//START ANGULARJS CURRICULAR AREA CONTROLLER
app.controller('CurricularAreaCreateController', function ($scope, $http, $location, $window) {

    //CALLS
    getCurricularAreaRandomCode();
    getAreas();

    function getCurricularAreaRandomCode() {
        $http({
            method: "GET",
            url: "/Area/RandomNumber"
        }).then(function (response) {
            $scope.curricularAreaRandomCode = response.data;
            $("#curricularAreaCode").val($scope.curricularAreaRandomCode);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    function getAreas() {
        $http({
            method: "GET",
            url: "/Area/GetAreas"
        }).then(function (response) {
            $scope.areas = response.data;
            console.log($scope.areas);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.createCurricularArea = function () {

        $scope.newCurricularArea = {};

        $scope.newCurricularArea.CurricularAreaCod = $scope.curricularAreaRandomCode;
        $scope.newCurricularArea.Name = $scope.curricularAreaName;
        $scope.newCurricularArea.AreaCod = $scope.Area.AreaCod;

        $http({
            method: "POST",
            url: "/Area/CreateCurricularArea",
            data: JSON.stringify($scope.newCurricularArea)
        }).then(function (response) {
            location.reload();
        }, function (error) {
            //swal('Error', error.data, 'error');
        });
    };
});


app.controller('ViewAreaStudentsController', function ($scope, $http, $location, $window) {

    //CALLS
    getStudents();

    function getStudents() {
        $http({
            method: "GET",
            url: "/Student/GetAllStudents"
        }).then(function (response) {
            $scope.students = response.data;
            console.log($scope.students);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.getStudentInformationModal = function (student) {

        $scope.studentInformationModal = student;
    };

    $scope.openStudentModifyModal = function () {
        $('#studentModifyModal').modal('toggle');

    };

    $scope.editStudent = function (studentData) {

        $scope.studentEdit = studentData;

        $http({
            method: "POST",
            url: "/Student/EditStudent",
            data: JSON.stringify($scope.studentEdit)
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

    $scope.deleteStudent = function (studentId) {

        $http({
            method: "GET",
            url: "/Student/DeleteStudent?studentId=" + studentId
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

});
//END ANGULARJS CURRICULAR AREA CONTROLLER



//START ANGULARJS COMPETENCE CONTROLLER 
app.controller('CompetenceCreateController', function ($scope, $http, $location, $window) {

    //CALLS
    getCurricularAreaRandomCode();
    getCurricularAreas();
    getSessions();

    function getCurricularAreaRandomCode() {
        $http({
            method: "GET",
            url: "/Area/RandomNumber"
        }).then(function (response) {
            $scope.competenceRandomCode = response.data;
            $("#competenceCode").val($scope.competenceRandomCode);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    function getCurricularAreas() {
        $http({
            method: "GET",
            url: "/Area/GetCurricularAreas"
        }).then(function (response) {
            $scope.curricularAreas = response.data;
            console.log($scope.curricularAreas);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    function getSessions() {
        $http({
            method: "GET",
            url: "/Session/GetSessions"
        }).then(function (response) {
            $scope.sessions = response.data;
            console.log($scope.sessions);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }
});


app.controller('ViewAreaStudentsController', function ($scope, $http, $location, $window) {

    //CALLS
    getStudents();

    function getStudents() {
        $http({
            method: "GET",
            url: "/Student/GetAllStudents"
        }).then(function (response) {
            $scope.students = response.data;
            console.log($scope.students);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.getStudentInformationModal = function (student) {

        $scope.studentInformationModal = student;
    };

    $scope.openStudentModifyModal = function () {
        $('#studentModifyModal').modal('toggle');

    };

    $scope.editStudent = function (studentData) {

        $scope.studentEdit = studentData;

        $http({
            method: "POST",
            url: "/Student/EditStudent",
            data: JSON.stringify($scope.studentEdit)
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

    $scope.deleteStudent = function (studentId) {

        $http({
            method: "GET",
            url: "/Student/DeleteStudent?studentId=" + studentId
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

});
//END ANGULARJS COMPETENCE CONTROLLER



//START ANGULARJS INDICATOR CONTROLLER
app.controller('IndicatorCreateController', function ($scope, $http, $location, $window) {

    //CALLS
    getIndicatorRandomCode();
    getSpecificCompetences();

    function getIndicatorRandomCode() {
        $http({
            method: "GET",
            url: "/Area/RandomNumber"
        }).then(function (response) {
            $scope.indicatorRandomCode = response.data;
            $("#indicatorCode").val($scope.indicatorRandomCode);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    function getSpecificCompetences() {
        $http({
            method: "GET",
            url: "/Area/GetSpecificCompetences"
        }).then(function (response) {
            $scope.competences = response.data;
            console.log($scope.competences);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    
});


app.controller('ViewAreaStudentsController', function ($scope, $http, $location, $window) {

    //CALLS
    getStudents();

    function getStudents() {
        $http({
            method: "GET",
            url: "/Student/GetAllStudents"
        }).then(function (response) {
            $scope.students = response.data;
            console.log($scope.students);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.getStudentInformationModal = function (student) {

        $scope.studentInformationModal = student;
    };

    $scope.openStudentModifyModal = function () {
        $('#studentModifyModal').modal('toggle');

    };

    $scope.editStudent = function (studentData) {

        $scope.studentEdit = studentData;

        $http({
            method: "POST",
            url: "/Student/EditStudent",
            data: JSON.stringify($scope.studentEdit)
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

    $scope.deleteStudent = function (studentId) {

        $http({
            method: "GET",
            url: "/Student/DeleteStudent?studentId=" + studentId
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

});
//END ANGULARJS INDICATOR CONTROLLER



//START ANGULARJS SESSION CONTROLLER
app.controller('SessionCreateController', function ($scope, $http, $location, $window) {

    //CALLS
    getSessionRandomCode();
    getCourses();
    getProfessors();

    function getSessionRandomCode() {
        $http({
            method: "GET",
            url: "/Session/RandomNumber"
        }).then(function (response) {
            $scope.sessionRandomCode = response.data;
            $("#sessionCode").val($scope.sessionRandomCode);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    function getCourses() {
        $http({
            method: "GET",
            url: "/Course/GetAllCourses"
        }).then(function (response) {
            $scope.courses = response.data;
            console.log($scope.courses);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    function getProfessors() {
        $http({
            method: "GET",
            url: "/Session/GetSessionProfessors"
        }).then(function (response) {
            $scope.professors = response.data;
            console.log($scope.professors);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.createSession = function () {

        $scope.newSession = {};

        $scope.newSession.SessionCod = $scope.sessionRandomCode;
        $scope.newSession.Name = $scope.sessionName;
        $scope.newSession.CourseCod = $scope.Course.CourseCod;
        $scope.newSession.ProfessorCod = $scope.Professor.ProfessorCod;

        $http({
            method: "POST",
            url: "/Session/CreateSession",
            data: JSON.stringify($scope.newSession)
        }).then(function (response) {
            location.reload();
        }, function (error) {
            //swal('Error', error.data, 'error');
        });
    };
});


app.controller('ViewAllSessionsController', function ($scope, $http, $location, $window) {

    let studentCod = '';

    //CALLS
    getSessions();

    function getSessions() {
        $http({
            method: "GET",
            url: "/Session/GetAllSessions"
        }).then(function (response) {
            $scope.sessions = response.data;
            console.log($scope.sessions);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    function getStudentsSession() {
        $http({
            method: "GET",
            url: "/Session/GetStudentsSession?courseCod=" + $scope.sessionInformationModal.CourseCod
        }).then(function (response) {
            $scope.students = response.data;
            console.log($scope.students);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    function getCompetencesWithIndicators() {

        $http({
            method: "POST",
            url: "/Area/GetCompetencesWithIndicators",
            data: {
                'sessionCod': $scope.sessionInformationModal.SessionCod
            }
        }).then(function (response) {
            $scope.competencesWithIndicators = response.data;
            console.log($scope.competencesWithIndicators);
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }


    //START FUNCTION
    $scope.calculateAverage = function () {

        let arr = $('input[id=grades]').map(function () {
            return this.value;
        }).get();


        let indicatorsCalification = 0;

        for (var i = 0; i < arr.length; i++) {
            indicatorsCalification += parseInt(arr[i]);
        }

        let studentGrade = indicatorsCalification / arr.length;
        let studentGradeRounded = Math.trunc(studentGrade);

        console.log(studentGradeRounded);
        saveStudentGrade(studentGradeRounded);
    };
    //END FUNCTION

    function saveStudentGrade(studentGradeRounded) {

        studentDetails = {};

        studentDetails.StudentCod = studentCod;
        studentDetails.Grade = studentGradeRounded;
        studentDetails.SessionCod = $scope.sessionInformationModal.SessionCod;

        $http({
            method: "POST",
            url: "/Student/SaveStudentGrade",
            data: JSON.stringify(studentDetails)
        }).then(function (response) {
            swal('Éxito', "Calificación guardada", 'success');
        }, function (error) {
            swal('Error', error.data, 'error');
        });
    }

    $scope.getSessionInformationModal = function (session) {

        $scope.sessionInformationModal = session;
    };

    $scope.openSessionModifyModal = function () {
        getStudentsSession();
        $('#sessionModifyModal').modal('toggle');

    };

    $scope.openGradesModal = function (studentCodParam) {
        studentCod = studentCodParam;
        getCompetencesWithIndicators();
        $('#gradesModal').modal('toggle');

    };

    $scope.editStudent = function (studentData) {

        $scope.studentEdit = studentData;

        $http({
            method: "POST",
            url: "/Student/EditStudent",
            data: JSON.stringify($scope.studentEdit)
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

    $scope.deleteStudent = function (studentId) {

        $http({
            method: "GET",
            url: "/Student/DeleteStudent?studentId=" + studentId
        }).then(function (response) {
            swal(response.data.message, response.data.status, "success");
        }, function (error) {
            swal(error.data.message, error.data.status, "error");
        });
    };

});
//END ANGULARJS SESSION CONTROLLER