﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sistema_Evaluacion.Models
{
    public class Util
    {
        [HttpGet]
        public int GenerateRandomNumber()
        {
            Random rnd = new Random();

            var randomCode = rnd.Next(1000, 9999);

            return randomCode;
        }
    }
}