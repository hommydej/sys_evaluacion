﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sistema_Evaluacion.Models
{
    public class BaseEntity
    {
        public int ID { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;

        public int CreatedBy { get; set; }

        public int Status { get; set; } = 1;
    }
}