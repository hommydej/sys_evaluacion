﻿using Sistema_Evaluacion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sistema_Evaluacion.Controllers
{
    public class ModulesController : Controller
    {
        SistemaEvaluacionEntities sistemaEvaluacionEntities;

        public ModulesController()
        {
            sistemaEvaluacionEntities = new SistemaEvaluacionEntities();
        }

        // GET: Modules
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetModules()
        {
            var accesos = Session["accesos"].ToString();
            var x = accesos.Split(',');
            var modulos = sistemaEvaluacionEntities.Modules.ToList();
            List<Modules> modulosAMostrar = new List<Modules>();

            foreach (var modulo in modulos)
            {
                foreach (var item in x)
                {
                    if (modulo.ModuleId == item)
                    {
                        modulosAMostrar.Add(new Modules
                        {
                            Id = modulo.Id,
                            Name = modulo.Name,
                            Url = modulo.Url,
                            ModuleId = modulo.ModuleId,
                            Icon = modulo.Icon
                        });
                    }
                }
            }
            return new JsonResult { Data = modulosAMostrar, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}