﻿using Sistema_Evaluacion.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Sistema_Evaluacion.Controllers
{
    public class CourseController : Controller
    {
        SistemaEvaluacionEntities sistemaEvaluacionEntities;
        Util util;

        public CourseController()
        {
            util = new Util();
            sistemaEvaluacionEntities = new SistemaEvaluacionEntities();
        }

        // GET: Course
        public ActionResult Create()
        {
            if (Session["usuario"] != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Login");
            }
        }

        public ActionResult ViewAll()
        {
            if (Session["usuario"] != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Login");
            }
        }

        [HttpPost]
        public string CreateCourse(Courses course)
        {
            var existingCourse = sistemaEvaluacionEntities.Courses.Where(c => c.CourseCod == course.CourseCod).FirstOrDefault();

            if (existingCourse == null)
            {
                try
                {
                    sistemaEvaluacionEntities.Courses.Add(course);
                    sistemaEvaluacionEntities.SaveChanges();
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }
            else
            {
                return "El código del curso ya está en uso";
            }

            return "Datos Guardados";
        }

        [HttpGet]
        public JsonResult GetAllCourses()
        {
            List<Courses> coursesList = sistemaEvaluacionEntities.Courses.Where(s => s.Status == 1).ToList();

            var subCategoryToReturn = coursesList.Select(s => new Courses
            {
                ID = s.ID,
                CourseCod = s.CourseCod,
                Name = s.Name,
            });

            if (subCategoryToReturn == null)
            {
                return new JsonResult { Data = "No hay Cursos Registrados", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = subCategoryToReturn, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpGet]
        public JsonResult SubjectCourseByCourseCod(int courseCod)
        {
            sistemaEvaluacionEntities.Configuration.ProxyCreationEnabled = false;

            var course = sistemaEvaluacionEntities.Courses.Where(s => s.CourseCod == courseCod && s.Status == 1).ToList();

            return new JsonResult { Data = course, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public JsonResult EditCourse(Courses course)
        {
            if (course != null)
            {
                var currentCourse = sistemaEvaluacionEntities.Courses.Where(a => a.ID == course.ID).FirstOrDefault();

                currentCourse.Name = course.Name;
                currentCourse.UpdatedBy = Convert.ToInt32(Session["CodUser"]);

                try
                {
                    sistemaEvaluacionEntities.SaveChanges();
                }
                catch
                {
                    return Json(new { message = "Error", status = "Ha Ocurrido un Error al Actualizar el Curso" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { message = "Éxito", status = "Curso Modificado Correctamente" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteCourse(int courseId)
        {
            var currentCourse = sistemaEvaluacionEntities.Courses.Where(a => a.ID == courseId).FirstOrDefault();

            if (currentCourse != null)
            {
                try
                {
                    currentCourse.Status = 0;

                    sistemaEvaluacionEntities.Entry(currentCourse).State = EntityState.Modified;
                    sistemaEvaluacionEntities.SaveChanges();
                }
                catch
                {
                    return Json(new { message = "Error", status = "Hubo un Error al Eliminar el Curso" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { message = "Éxito", status = "Curso Eliminado con Éxito" }, JsonRequestBehavior.AllowGet);
        }

        public int RandomNumber()
        {
            var randomNumber = util.GenerateRandomNumber();
            return randomNumber;
        }
    }
}