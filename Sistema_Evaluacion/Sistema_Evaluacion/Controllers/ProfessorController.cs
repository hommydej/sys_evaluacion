﻿using Sistema_Evaluacion.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Sistema_Evaluacion.Controllers
{
    public class ProfessorController : Controller
    {
        SistemaEvaluacionEntities sistemaEvaluacionEntities;
        Util util;

        public ProfessorController()
        {
            util = new Util();
            sistemaEvaluacionEntities = new SistemaEvaluacionEntities();
        }

        // GET: Professor
        public ActionResult Create()
        {
            if (Session["usuario"] != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Login");
            }
        }

        public ActionResult ViewAll()
        {
            if (Session["usuario"] != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Login");
            }
        }

        public ActionResult ViewProfile()
        {
            if (Session["usuario"] != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Login");
            }
        }

        [HttpPost]
        public string CreateProfessor(Professors professor)
        {
            var existingProfessor = sistemaEvaluacionEntities.Professors.Where(c => c.ProfessorCod == professor.ProfessorCod).FirstOrDefault();
            
            if (existingProfessor == null)
            {
                try
                {
                    sistemaEvaluacionEntities.Professors.Add(professor);
                    sistemaEvaluacionEntities.SaveChanges();
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }
            else
            {
                return "El código del profesor ya está en uso";
            }
            
            return "Datos Guardados";
        }

        [HttpGet]
        public JsonResult GetAllProfessors()
        {
            List<Professors> professorsList = sistemaEvaluacionEntities.Professors.Where(s => s.Status == 1 && s.Assigned != true).ToList();

            var subCategoryToReturn = professorsList.Select(s => new Professors
            {
                ID              = s.ID,
                Name            = s.Name,
                LastName        = s.LastName,
                DateOfBirth     = s.DateOfBirth,
                Identification  = s.Identification,
                TelephoneNumber = s.TelephoneNumber,
                CellPhoneNumber = s.CellPhoneNumber,
                Gender          = s.Gender,
                Address         = s.Address,
                City            = s.City,
                CountryOfBirth  = s.CountryOfBirth,
                Photo           = s.Photo,
                ProfessorCod    = s.ProfessorCod,
                Password        = s.Password,
            });

            if (subCategoryToReturn == null)
            {
                return new JsonResult { Data = "No hay profesores registrados", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = subCategoryToReturn, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public JsonResult EditProfessor(Professors professor)
        {
            if (professor != null)
            {
                var currentProfessor = sistemaEvaluacionEntities.Professors.Where(a => a.ID == professor.ID).FirstOrDefault();

                currentProfessor.Name            = professor.Name;
                currentProfessor.LastName        = professor.LastName;
                currentProfessor.DateOfBirth     = professor.DateOfBirth;
                currentProfessor.Identification  = professor.Identification;
                currentProfessor.TelephoneNumber = professor.TelephoneNumber;
                currentProfessor.CellPhoneNumber = professor.CellPhoneNumber;
                currentProfessor.Gender          = professor.Gender;
                currentProfessor.Address         = professor.Address;
                currentProfessor.City            = professor.City;
                currentProfessor.CountryOfBirth  = professor.CountryOfBirth;
                currentProfessor.Photo           = professor.Photo;
                currentProfessor.ProfessorCod    = professor.ProfessorCod;
                currentProfessor.Password        = professor.TelephoneNumber;
                currentProfessor.UpdatedBy       = Convert.ToInt32(Session["CodUser"]);

                try
                {
                    sistemaEvaluacionEntities.SaveChanges();
                }
                catch
                {
                    return Json(new { message = "Error", status = "Ha Ocurrido un Error al Actualizar el Profesor" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { message = "Éxito", status = "Profesor Modificado Correctamente" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteProfessor(int professorId)
        {
            var currentProfessor = sistemaEvaluacionEntities.Professors.Where(a => a.ID == professorId).FirstOrDefault();

            if (currentProfessor != null)
            {
                try
                {
                    currentProfessor.Status = 0;

                    sistemaEvaluacionEntities.Entry(currentProfessor).State = EntityState.Modified;
                    sistemaEvaluacionEntities.SaveChanges();
                }
                catch
                {
                    return Json(new { message = "Error", status = "Hubo un Error al Eliminar el Profesor" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { message = "Éxito", status = "Profesor Eliminado con Éxito" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public void UpdateAssignedProfessorFlag(int professorId)
        {
            var professor = sistemaEvaluacionEntities.Professors.Where(p => p.ProfessorCod == professorId).FirstOrDefault();

            professor.Assigned = true;

            sistemaEvaluacionEntities.SaveChanges();
        }

        public int RandomNumber()
        {
            var randomNumber = util.GenerateRandomNumber();
            return randomNumber;
        }

    }
}