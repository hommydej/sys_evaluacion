﻿using Sistema_Evaluacion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sistema_Evaluacion.Controllers
{
    public class UserController : Controller
    {
        SistemaEvaluacionEntities sistemaEvaluacionEntities;
        Util util;

        public UserController()
        {
            util = new Util();
            sistemaEvaluacionEntities = new SistemaEvaluacionEntities();
        }

        // GET: User
        public ActionResult ViewAll()
        {
            if (Session["usuario"] != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Login");
            }
        }

        public ActionResult Create()
        {
            if (Session["usuario"] != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Login");
            }
        }

        public JsonResult GetAll()
        {
            var admins = sistemaEvaluacionEntities.AdminUsers.Where(a => a.Status == 1).ToList();

            return new JsonResult { Data = admins, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public string CreateUser(AdminUsers user)
        {
            var existingUser = sistemaEvaluacionEntities.AdminUsers.Where(c => c.CodUser == user.CodUser).FirstOrDefault();

            if (existingUser == null)
            {
                try
                {
                    sistemaEvaluacionEntities.AdminUsers.Add(user);
                    sistemaEvaluacionEntities.SaveChanges();
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }
            else
            {
                return "El código del usuario ya está en uso";
            }

            return "Datos Guardados";
        }

        public int RandomNumber()
        {
            var randomNumber = util.GenerateRandomNumber();
            return randomNumber;
        }
    }
}