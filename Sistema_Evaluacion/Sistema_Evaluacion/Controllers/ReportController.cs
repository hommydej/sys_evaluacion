﻿using Sistema_Evaluacion.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sistema_Evaluacion.Controllers
{
    public class ReportController : Controller
    {
        SistemaEvaluacionEntities sistemaEvaluacionEntities;

        public ReportController()
        {
            sistemaEvaluacionEntities = new SistemaEvaluacionEntities();
        }
        // GET: Report
        public ActionResult SpecificReport()
        {
            if (Session["usuario"] != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Login");
            }
        }

        public ActionResult GeneralReport()
        {
            if (Session["usuario"] != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Login");
            }
        }

        public JsonResult GetStudentSpecificGradeReport(int studentCod)
        {
            sistemaEvaluacionEntities.Configuration.ProxyCreationEnabled = false;
            //var result = sistemaEvaluacionEntities.StudentGrades.Where(s => s.StudentCod == studentCod && s.CourseCod == courseCod).Select(s => new { s.Students, s.Courses, s.Grade}).ToList();

            var studentName        = sistemaEvaluacionEntities.Students.Where(s => s.StudentCod == studentCod).Select(n => n.Name).FirstOrDefault();
            var sessionsWithGrades = sistemaEvaluacionEntities.StudentGrades.Where(s => s.StudentCod == studentCod).Select(s => new { s.Grade, s.Subject.Name }).ToList();

            var result = new
            {
                StudentName = studentName,
                SubjectsAndGrades = sessionsWithGrades
            };

            return new JsonResult { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}