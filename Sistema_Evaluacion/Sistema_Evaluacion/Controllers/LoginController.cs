﻿using Sistema_Evaluacion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sistema_Evaluacion.Controllers
{
    public class LoginController : Controller
    {
        SistemaEvaluacionEntities sistemaEvaluacionEntities;

        public LoginController()
        {
            sistemaEvaluacionEntities = new SistemaEvaluacionEntities();
        }

        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public int GetUserType(int userCod)
        {
            //Obtenemos el tipo de usuario del profesor
            var userType = sistemaEvaluacionEntities.Professors.Where(c => c.ProfessorCod == userCod).Select(u => u.UserType).FirstOrDefault();

            return userType;
        }
        [HttpPost]
        public int Login(int userCod, string password)
        {
            var userType = GetUserType(userCod);

            if (userType == 2)
            {
                var loggedUser = sistemaEvaluacionEntities.Professors.Where(u => u.ProfessorCod == userCod && u.Password == password).FirstOrDefault();

                if (loggedUser != null)
                {
                    Session["usuario"] = loggedUser.ProfessorCod;
                    Session["nombre"] = loggedUser.Name + " " + loggedUser.LastName;
                    Session["accesos"] = loggedUser.Accesos;

                    return 1;
                }
                return 0;
            }
            else
            {
                var loggedUser = sistemaEvaluacionEntities.AdminUsers.Where(u => u.CodUser == userCod && u.Password == password).FirstOrDefault();

                if (loggedUser != null)
                {
                    Session["usuario"] = loggedUser.CodUser;
                    Session["accesos"] = loggedUser.Accesos;

                    return 1;
                }
                return 0;
            }
        }

        public ActionResult LogOut()
        {
            Session["usuario"] = null;
            Session["nombre"] = null;
            Session["accesos"] = null;

            return Redirect("/Login");
        }
    }
}