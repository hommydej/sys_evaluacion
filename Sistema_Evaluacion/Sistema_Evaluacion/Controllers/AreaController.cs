﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sistema_Evaluacion.Models;

namespace Sistema_Evaluacion.Controllers
{
    //public class AreaController : Controller
    //{
    //    Util util;
    //    SistemaEvaluacionEntities sistemaEvaluacionEntities;

    //    public AreaController()
    //    {
    //        util = new Util();
    //        sistemaEvaluacionEntities = new SistemaEvaluacionEntities();
    //    }

    //    // GET: Area
    //    public ActionResult Create()
    //    {
    //        if (Session["usuario"] != null)
    //        {
    //            return View();
    //        }
    //        else
    //        {
    //            return Redirect("/Login");
    //        }
    //    }

    //    public ActionResult CreateCurricularArea()
    //    {
    //        if (Session["usuario"] != null)
    //        {
    //            return View();
    //        }
    //        else
    //        {
    //            return Redirect("/Login");
    //        }
    //    }

    //    public ActionResult CreateCompetence()
    //    {
    //        if (Session["usuario"] != null)
    //        {
    //            return View();
    //        }
    //        else
    //        {
    //            return Redirect("/Login");
    //        }
    //    }

    //    public ActionResult CreateIndicator()
    //    {
    //        if (Session["usuario"] != null)
    //        {
    //            return View();
    //        }
    //        else
    //        {
    //            return Redirect("/Login");
    //        }
    //    }

    //    public JsonResult GetAreas()
    //    {
    //        List<Area> areasList = sistemaEvaluacionEntities.Area.Where(s => s.Status == 1).ToList();

    //        var subCategoryToReturn = areasList.Select(s => new Area
    //        {
    //            ID = s.ID,
    //            Name = s.Name,
    //            AreaCod = s.AreaCod
    //        });

    //        if (subCategoryToReturn == null)
    //        {
    //            return new JsonResult { Data = "No hay áreas registradas", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
    //        }

    //        return new JsonResult { Data = subCategoryToReturn, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
    //    }

    //    public JsonResult GetCurricularAreas()
    //    {
    //        List<CurricularArea> curricularAreasList = sistemaEvaluacionEntities.CurricularArea.Where(s => s.Status == 1).ToList();

    //        var subCategoryToReturn = curricularAreasList.Select(s => new CurricularArea
    //        {
    //            ID = s.ID,
    //            CurricularAreaCod = s.CurricularAreaCod,
    //            Name = s.Name,

    //        });

    //        if (subCategoryToReturn == null)
    //        {
    //            return new JsonResult { Data = "No hay áreas curriculares registradas", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
    //        }

    //        return new JsonResult { Data = subCategoryToReturn, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
    //    }
    //    [HttpGet]
    //    public JsonResult GetSpecificCompetences()
    //    {
    //        List<Competences> competencesList = sistemaEvaluacionEntities.Competences.Where(s => s.Status == 1 && s.IsGeneral == false).ToList();

    //        var subCategoryToReturn = competencesList.Select(s => new Competences
    //        {
    //            ID = s.ID,
    //            CompetenceCod = s.CompetenceCod,
    //            Name = s.Name,
    //            IsGeneral = s.IsGeneral

    //        });

    //        if (subCategoryToReturn == null)
    //        {
    //            return new JsonResult { Data = "No hay competencias registradas", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
    //        }

    //        return new JsonResult { Data = subCategoryToReturn, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
    //    }
        
    //    public JsonResult GetCompetencesWithIndicators(int sessionCod)
    //    {
    //        sistemaEvaluacionEntities.Configuration.ProxyCreationEnabled = false;

    //        List<string> generalCompetences = sistemaEvaluacionEntities.Competences.Where(s => s.Status == 1 && s.IsGeneral == true).Select(n => n.Name).ToList();
    //        var specificCompetences = sistemaEvaluacionEntities.Competences.Where(s => s.SessionCod == sessionCod).Include(s => s.Indicators).Select(s => new { s.Name, s.Indicators }).ToList();

    //        var result = new
    //        {
    //            GeneralCompetences  = generalCompetences,
    //            SpecificCompetences = specificCompetences
    //        };

    //        return new JsonResult { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
    //    }

    //    public JsonResult GetSpecificCompetencesBySessionCod(int sessionCod)
    //    {
    //        List<Competences> competencesList = sistemaEvaluacionEntities.Competences.Where(s => s.Status == 1 && s.IsGeneral == false && s.SessionCod == sessionCod).ToList();

    //        var subCategoryToReturn = competencesList.Select(s => new Competences
    //        {
    //            ID = s.ID,
    //            CompetenceCod = s.CompetenceCod,
    //            Name = s.Name,
    //            IsGeneral = s.IsGeneral

    //        });

    //        if (subCategoryToReturn == null)
    //        {
    //            return new JsonResult { Data = "No hay competencias registradas", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
    //        }

    //        return new JsonResult { Data = subCategoryToReturn, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
    //    }

    //    public JsonResult GetGeneralCompetences()
    //    {
    //        sistemaEvaluacionEntities.Configuration.ProxyCreationEnabled = false;

    //        var competences = sistemaEvaluacionEntities.Competences.Where(s => s.IsGeneral == true).ToList();

    //        return new JsonResult { Data = competences, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
    //    }

    //    public JsonResult GetIndicators(int competenceCod)
    //    {
    //        List<Indicators> indicatorsList = sistemaEvaluacionEntities.Indicators.Where(s => s.Status == 1 && s.CompetenceCod == competenceCod).ToList();

    //        var subCategoryToReturn = indicatorsList.Select(s => new Indicators
    //        {
    //            ID = s.ID,
    //            CompetenceCod = s.CompetenceCod,
    //            IndicatorCod = s.IndicatorCod,
    //            Name = s.Name
    //        });

    //        if (subCategoryToReturn == null)
    //        {
    //            return new JsonResult { Data = "No hay indicadores registrados", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
    //        }

    //        return new JsonResult { Data = subCategoryToReturn, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
    //    }

    //    [HttpPost]
    //    public string CreateArea(Area area)
    //    {
    //        var existingArea = sistemaEvaluacionEntities.Area.Where(c => c.AreaCod == area.AreaCod).FirstOrDefault();

    //        if (existingArea == null)
    //        {
    //            try
    //            {
    //                sistemaEvaluacionEntities.Area.Add(area);
    //                sistemaEvaluacionEntities.SaveChanges();
    //            }
    //            catch (Exception ex)
    //            {
    //                return ex.ToString();
    //            }
    //        }
    //        else
    //        {
    //            return "El código del área ya está en uso";
    //        }

    //        return "Datos Guardados";
    //    }

    //    [HttpPost]
    //    public string CreateCurricularArea(CurricularArea curricularArea)
    //    {
    //        var existingCurricularArea = sistemaEvaluacionEntities.CurricularArea.Where(c => c.CurricularAreaCod == curricularArea.CurricularAreaCod).FirstOrDefault();

    //        if (existingCurricularArea == null)
    //        {
    //            try
    //            {
    //                sistemaEvaluacionEntities.CurricularArea.Add(curricularArea);
    //                sistemaEvaluacionEntities.SaveChanges();
    //            }
    //            catch (Exception ex)
    //            {
    //                return ex.ToString();
    //            }
    //        }
    //        else
    //        {
    //            return "El código del área curricular ya está en uso";
    //        }

    //        return "Datos Guardados";
    //    }

    //    [HttpPost]
    //    public string CreateCompetence(Competences competence)
    //    {
    //        var existingCompetence = sistemaEvaluacionEntities.Competences.Where(c => c.CompetenceCod == competence.CompetenceCod).FirstOrDefault();

    //        if (existingCompetence == null)
    //        {
    //            try
    //            {
    //                sistemaEvaluacionEntities.Competences.Add(competence);
    //                sistemaEvaluacionEntities.SaveChanges();
    //            }
    //            catch (Exception ex)
    //            {
    //                return ex.ToString();
    //            }
    //        }
    //        else
    //        {
    //            return "El código de la competencia ya está en uso";
    //        }

    //        return "Datos Guardados";
    //    }

    //    [HttpPost]
    //    public string CreateIndicator(Indicators indicator)
    //    {
    //        var existingIndicator = sistemaEvaluacionEntities.Indicators.Where(c => c.IndicatorCod == indicator.IndicatorCod).FirstOrDefault();

    //        if (existingIndicator == null)
    //        {
    //            try
    //            {
    //                sistemaEvaluacionEntities.Indicators.Add(indicator);
    //                sistemaEvaluacionEntities.SaveChanges();
    //            }
    //            catch (Exception ex)
    //            {
    //                return ex.ToString();
    //            }
    //        }
    //        else
    //        {
    //            return "El código del indicador ya está en uso";
    //        }

    //        return "Datos Guardados";
    //    }

    //    public int RandomNumber()
    //    {
    //        var randomNumber = util.GenerateRandomNumber();
    //        return randomNumber;
    //    }
    //}
}