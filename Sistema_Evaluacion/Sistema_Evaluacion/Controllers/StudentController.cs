﻿using Sistema_Evaluacion.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sistema_Evaluacion.Controllers
{
    public class StudentController : Controller
    {
        SistemaEvaluacionEntities sistemaEvaluacionEntities;
        Util util;

        public StudentController()
        {
            util = new Util();
            sistemaEvaluacionEntities = new SistemaEvaluacionEntities();
        }

        // GET: Student
        public ActionResult Create()
        {
            if (Session["usuario"] != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Login");
            }
        }

        public ActionResult ViewAll()
        {
            if (Session["usuario"] != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Login");
            }
        }

        [HttpPost]
        public string CreateStudent(Students student)
        {
            var existingStudent = sistemaEvaluacionEntities.Students.Where(c => c.StudentCod == student.StudentCod).FirstOrDefault();

            if (existingStudent == null)
            {
                try
                {
                    student.CreatedBy = (int)Session["usuario"];

                    sistemaEvaluacionEntities.Students.Add(student);
                    sistemaEvaluacionEntities.SaveChanges();
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }
            else
            {
                return "El código del estudiante ya está en uso";
            }

            return "Datos Guardados";
        }

        [HttpGet]
        public JsonResult GetAllStudents()
        {
            List<Students> studentList = sistemaEvaluacionEntities.Students.Where(s => s.Status == 1).ToList();

            var subCategoryToReturn = studentList.Select(s => new Students
            {
                ID              = s.ID,
                StudentCod      = s.StudentCod,
                Name            = s.Name,
                LastName        = s.LastName,
                DateOfBirth     = s.DateOfBirth,
                Gender          = s.Gender,
                SessionsCod     = s.SessionsCod,
                Address         = s.Address,
            });

            if (subCategoryToReturn == null)
            {
                return new JsonResult { Data = "No hay estudiantes registrados", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = subCategoryToReturn, JsonRequestBehavior = JsonRequestBehavior.AllowGet };   
        }

        [HttpGet]
        public JsonResult GetBySession(string SessionCod)
        {
            List<Students> studentList = sistemaEvaluacionEntities.Students.Where(s => s.SessionsCod.Contains(SessionCod) &&  s.Status == 1).ToList();

            var subCategoryToReturn = studentList.Select(s => new Students
            {
                ID = s.ID,
                StudentCod = s.StudentCod,
                Name = s.Name,
                LastName = s.LastName,
                DateOfBirth = s.DateOfBirth,
                Gender = s.Gender,
                Address = s.Address
            });

            if (subCategoryToReturn == null)
            {
                return new JsonResult { Data = "No hay estudiantes registrados", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = subCategoryToReturn, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult SaveStudentGrade(StudentGrades studentGrades)
        {
            try
            {
                studentGrades.CreatedBy = (int)Session["usuario"];

                sistemaEvaluacionEntities.StudentGrades.Add(studentGrades);

                sistemaEvaluacionEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                return new JsonResult { Data = ex, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = studentGrades, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public JsonResult EditStudent(Students student)
        {
            if (student != null)
            {
                var currentStudent = sistemaEvaluacionEntities.Students.Where(a => a.ID == student.ID).FirstOrDefault();

                currentStudent.Name            = student.Name;
                currentStudent.LastName        = student.LastName;
                currentStudent.DateOfBirth     = student.DateOfBirth;
                currentStudent.Gender          = student.Gender;
                currentStudent.UpdatedBy       = (int)Session["usuario"];

                try
                {
                    sistemaEvaluacionEntities.SaveChanges();
                }
                catch
                {
                    return Json(new { message = "Error", status = "Ha Ocurrido un Error al Actualizar el Estudiante" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { message = "Éxito", status = "Estudiante Modificado Correctamente" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteStudent(int studentId)
        {
            var currentStudent = sistemaEvaluacionEntities.Students.Where(a => a.ID == studentId).FirstOrDefault();

            if (currentStudent != null)
            {
                try
                {
                    currentStudent.Status = 0;

                    sistemaEvaluacionEntities.Entry(currentStudent).State = EntityState.Modified;
                    sistemaEvaluacionEntities.SaveChanges();
                }
                catch
                {
                    return Json(new { message = "Error", status = "Hubo un Error al Eliminar el Estudiante" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { message = "Éxito", status = "Estudiante Eliminado con Éxito" }, JsonRequestBehavior.AllowGet);
        }

        public int RandomNumber()
        {
            var randomNumber = util.GenerateRandomNumber();
            return randomNumber;
        }
    }
}