﻿using Sistema_Evaluacion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sistema_Evaluacion.Controllers
{
    public class IndicatorController : Controller
    {
        SistemaEvaluacionEntities sistemaEvaluacionEntities;
        Util util;

        public IndicatorController()
        {
            util = new Util();
            sistemaEvaluacionEntities = new SistemaEvaluacionEntities();
        }

        // GET: Indicators
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetIndicatorsByCompetenceCod(int competenceCod)
        {
            List<Indicators> indicatorsList = sistemaEvaluacionEntities.Indicators.Where(s => s.Status == 1 && s.CompetenceCod == competenceCod).ToList();

            var subCategoryToReturn = indicatorsList.Select(s => new Indicators
            {
                ID              = s.ID,
                CompetenceCod   = s.CompetenceCod,
                IndicatorCod    = s.IndicatorCod,
                Name            = s.Name
            });

            if (subCategoryToReturn == null)
            {
                return new JsonResult { Data = "No hay indicadores registrados", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = subCategoryToReturn, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public string Create(Indicators indicator)
        {
            var existingIndicator = sistemaEvaluacionEntities.Indicators.Where(c => c.IndicatorCod == indicator.IndicatorCod).FirstOrDefault();

            if (existingIndicator == null)
            {
                try
                {
                    indicator.CreatedBy = Convert.ToInt32(Session["usuario"]);

                    sistemaEvaluacionEntities.Indicators.Add(indicator);
                    sistemaEvaluacionEntities.SaveChanges();
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }
            else
            {
                return "El código del indicador ya está en uso";
            }

            return "Datos Guardados";
        }

        [HttpPost]
        public JsonResult Edit(Indicators indicator)
        {
            if (indicator != null)
            {
                var currentIndicator = sistemaEvaluacionEntities.Indicators.Where(a => a.ID == indicator.ID).FirstOrDefault();

                currentIndicator.Name      = indicator.Name;
                currentIndicator.UpdatedBy = (int)Session["usuario"];

                try
                {
                    sistemaEvaluacionEntities.SaveChanges();
                }
                catch
                {
                    return Json(new { status = "Ha ocurrido un error al actualizar el indicador" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { status = "Indicador modificado correctamente" }, JsonRequestBehavior.AllowGet);
        }

        public int RandomNumber()
        {
            var randomNumber = util.GenerateRandomNumber();
            return randomNumber;
        }
    }
}