﻿using Sistema_Evaluacion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sistema_Evaluacion.Controllers
{
    public class SubjectController : Controller
    {
        Util util;
        SistemaEvaluacionEntities sistemaEvaluacionEntities;
        
        public SubjectController()
        {
            util = new Util();
            sistemaEvaluacionEntities = new SistemaEvaluacionEntities();
        }

        [HttpPost]
        public string Create(Subject subject)
        {
            var existingSubject = sistemaEvaluacionEntities.Subject.Where(c => c.SubjectCod == subject.SubjectCod).FirstOrDefault();

            if (existingSubject == null)
            {
                try
                {
                    sistemaEvaluacionEntities.Subject.Add(subject);
                    sistemaEvaluacionEntities.SaveChanges();
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }
            else
            {
                return "El código de la materia ya está en uso";
            }

            return "Datos Guardados";
        }

        [HttpGet]
        public JsonResult GetByCourseId(int courseCod)
        {
            sistemaEvaluacionEntities.Configuration.ProxyCreationEnabled = false;

            var subjects = sistemaEvaluacionEntities.Subject.Where(c => c.CourseCod == courseCod && c.Status == 1).ToList();
            
            return new JsonResult { Data = subjects, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpGet]
        public JsonResult SubjectSessionBySubjectCod(int subjectCod)
        {
            sistemaEvaluacionEntities.Configuration.ProxyCreationEnabled = false;

            var subject = sistemaEvaluacionEntities.Subject.Where(c => c.SubjectCod == subjectCod && c.Status == 1).ToList();

            return new JsonResult { Data = subject, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public int RandomNumber()
        {
            var randomNumber = util.GenerateRandomNumber();
            return randomNumber;
        }
    }
}