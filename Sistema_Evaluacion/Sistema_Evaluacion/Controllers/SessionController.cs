﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sistema_Evaluacion.Models;

namespace Sistema_Evaluacion.Controllers
{
    public class SessionController : Controller
    {
        SistemaEvaluacionEntities sistemaEvaluacionEntities;
        Util util;

        public SessionController()
        {
            sistemaEvaluacionEntities = new SistemaEvaluacionEntities();
            util = new Util();
        }

        [HttpGet]
        public JsonResult GetSessions()
        {
            List<Session> sessionList = sistemaEvaluacionEntities.Session.Where(s => s.Status == 1).ToList();

            var subCategoryToReturn = sessionList.Select(s => new Session
            {
                ID = s.ID,
                SessionCod = s.SessionCod,
                Name = s.Name
            });

            if (subCategoryToReturn == null)
            {
                return new JsonResult { Data = "No hay Sesiones Registradas", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = subCategoryToReturn, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpGet]
        public JsonResult ByProfessorCod()
        {
            if (Session["usuario"] != null)
            {
                Redirect("/Login");
            }

            var professorCod = (int)Session["usuario"];

            sistemaEvaluacionEntities.Configuration.ProxyCreationEnabled = false;

            List<Session> sessionsList = sistemaEvaluacionEntities.Session.Where(p => p.ProfessorCod == professorCod).ToList();

            return new JsonResult { Data = sessionsList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public string Create(Session session)
        {
            var existingSession = sistemaEvaluacionEntities.Session.Where(c => c.SessionCod == session.SessionCod).FirstOrDefault();

            if (existingSession == null)
            {
                try
                {
                    sistemaEvaluacionEntities.Session.Add(session);
                    sistemaEvaluacionEntities.SaveChanges();
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }
            else
            {
                return "El código de la sesión ya está en uso";
            }

            return "Datos Guardados";
        }

        [HttpGet]
        public JsonResult GetSessionProfessors()
        {
            List<Professors> professorsList = sistemaEvaluacionEntities.Professors.Where(s => s.Status == 1).ToList();

            var subCategoryToReturn = professorsList.Select(s => new Professors
            {
                ID = s.ID,
                Name = s.Name,
                LastName = s.LastName,
                DateOfBirth = s.DateOfBirth,
                Identification = s.Identification,
                TelephoneNumber = s.TelephoneNumber,
                CellPhoneNumber = s.CellPhoneNumber,
                Gender = s.Gender,
                Address = s.Address,
                City = s.City,
                CountryOfBirth = s.CountryOfBirth,
                Photo = s.Photo,
                ProfessorCod = s.ProfessorCod,
                Password = s.Password,
            });

            if (subCategoryToReturn == null)
            {
                return new JsonResult { Data = "No hay profesores registrados", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = subCategoryToReturn, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpGet]
        public JsonResult GetAllSessions()
        {
            var userAccess = Session["accesos"].ToString();
            var currUser = (int)Session["usuario"];

            sistemaEvaluacionEntities.Configuration.ProxyCreationEnabled = false;

            List<Session> sessionsList = new List<Session>();

            //VALIDACIÓN PARA PRIVILEGIOS DE USUARIO ADMIN
            if (userAccess == "0")
            {
                sessionsList = sistemaEvaluacionEntities.Session.Where(s => s.Status == 1).ToList();

                return new JsonResult { Data = sessionsList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            sessionsList = sistemaEvaluacionEntities.Session.Where(s => s.Status == 1 && s.ProfessorCod == currUser).ToList();

            return new JsonResult { Data = sessionsList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        //public JsonResult GetStudentsSession(int CourseCod)
        //{
        //    sistemaEvaluacionEntities.Configuration.ProxyCreationEnabled = false;

        //    List<object> studentsList = new List<object>();

        //    var studentsCod = sistemaEvaluacionEntities.CourseStudent.Where(c => c.CourseCod == CourseCod).Select(s => s.StudentCod).ToList();

        //    foreach (var item in studentsCod)
        //    {
        //        var student = sistemaEvaluacionEntities.Students.Where(s => s.StudentCod == item).ToList();

        //        studentsList.Add(student);
        //    }

        //    return new JsonResult { Data = studentsList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        //}

        public JsonResult GetSessionsBySubject(int subjectCod)
        {
            sistemaEvaluacionEntities.Configuration.ProxyCreationEnabled = false;

            var sessions = sistemaEvaluacionEntities.Session.Where(c => c.SubjectCod == subjectCod).ToList();

            return new JsonResult { Data = sessions, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetSubjectCodBySessionCod(int sessionCod)
        {
            sistemaEvaluacionEntities.Configuration.ProxyCreationEnabled = false;

            var subjectCod = sistemaEvaluacionEntities.Session.Where(c => c.SessionCod == sessionCod).Select(s => s.SubjectCod).FirstOrDefault();

            return new JsonResult { Data = subjectCod, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public int RandomNumber()
        {
            var randomNumber = util.GenerateRandomNumber();
            return randomNumber;
        }
    }
}