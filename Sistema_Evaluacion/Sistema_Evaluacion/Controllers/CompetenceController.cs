﻿using Sistema_Evaluacion.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sistema_Evaluacion.Controllers
{
    public class CompetenceController : Controller
    {
        SistemaEvaluacionEntities sistemaEvaluacionEntities;
        Util util;

        public CompetenceController()
        {
            util = new Util();
            sistemaEvaluacionEntities = new SistemaEvaluacionEntities();
        }

        // GET: Competence
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetCompetencesWithIndicators(int subjectCod)
        {
            sistemaEvaluacionEntities.Configuration.ProxyCreationEnabled = false;

            var competencesAndIndicators = sistemaEvaluacionEntities.Competences.Where(s => s.SubjectCod == subjectCod).Include(s => s.Indicators).Select(s => new { s.Name, s.Indicators }).ToList();

            return new JsonResult { Data = competencesAndIndicators, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpGet]
        public JsonResult BySubjectCod(int subjectCod)
        {
            List<Competences> competencesList = sistemaEvaluacionEntities.Competences.Where(s => s.SubjectCod == subjectCod && s.Status == 1).ToList();

            var subCategoryToReturn = competencesList.Select(s => new Competences
            {
                ID = s.ID,
                CompetenceCod = s.CompetenceCod,
                Name = s.Name

            });

            if (subCategoryToReturn == null)
            {
                return new JsonResult { Data = "No hay competencias registradas", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = subCategoryToReturn, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public string CreateCompetence(Competences competence)
        {
            var existingCompetence = sistemaEvaluacionEntities.Competences.Where(c => c.CompetenceCod == competence.CompetenceCod).FirstOrDefault();

            if (existingCompetence == null)
            {
                try
                {
                    competence.CreatedBy = Convert.ToInt32(Session["usuario"]);

                    sistemaEvaluacionEntities.Competences.Add(competence);
                    sistemaEvaluacionEntities.SaveChanges();
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }
            else
            {
                return "El código de la competencia ya está en uso";
            }

            return "Datos Guardados";
        }

        [HttpPost]
        public JsonResult Edit(Competences competence)
        {
            if (competence != null)
            {
                var currentCompetence = sistemaEvaluacionEntities.Competences.Where(a => a.ID == competence.ID).FirstOrDefault();

                currentCompetence.Name = competence.Name;
                currentCompetence.UpdatedBy = (int)Session["usuario"];

                try
                {
                    sistemaEvaluacionEntities.SaveChanges();
                }
                catch
                {
                    return Json(new { status = "Ha ocurrido un error al actualizar la competencia" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { status = "Competencia modificada correctamente" }, JsonRequestBehavior.AllowGet);
        }

        public int RandomNumber()
        {
            var randomNumber = util.GenerateRandomNumber();
            return randomNumber;
        }
    }
}